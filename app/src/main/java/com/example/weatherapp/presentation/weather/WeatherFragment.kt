package com.example.weatherapp.presentation.weather

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.weatherapp.R
import com.example.weatherapp.application.WeatherApp
import com.example.weatherapp.databinding.FragmentWeatherBinding
import com.example.weatherapp.domain.model.WeatherDataModel

class WeatherFragment : Fragment() {

    private val weatherViewModel: WeatherViewModel by viewModels {
        WeatherViewModelFactory(WeatherApp.instance.weatherRepository)
    }

    private var _binding: FragmentWeatherBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentWeatherBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        weatherViewModel.weatherLD
            .observe(viewLifecycleOwner, {
                when (it) {
                    is WeatherUiState.Error -> showErrorState(it.fail)
                    WeatherUiState.Loading -> showLoadingState()
                    is WeatherUiState.Success -> showDataState(it.data)
                }
            })

        binding.retryButton.setOnClickListener {
            weatherViewModel.reloadWeather()
        }
        binding.buttonSecond.setOnClickListener {
            findNavController().navigate(R.id.action_WeatherFragment_to_WelcomeFragment)
        }
    }

    private fun showDataState(data: WeatherDataModel) {
        binding.dataTextView.isVisible = true
        binding.dataTextView.text = data.toString()
        binding.errorView.isVisible = false
        binding.loadingView.isVisible = false
    }

    private fun showLoadingState() {
        binding.dataTextView.isVisible = false
        binding.errorView.isVisible = false
        binding.loadingView.isVisible = true
        showErrorState(Throwable("Something went wrong"))
    }

    private fun showErrorState(fail: Throwable) {
        binding.dataTextView.isVisible = false
        binding.errorView.isVisible = true
        binding.errorTextView.text = fail.message ?: "Unknown exception"
        binding.loadingView.isVisible = false

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}
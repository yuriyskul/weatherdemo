package com.example.weatherapp.presentation.weather

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.weatherapp.domain.model.WeatherDataModel
import com.example.weatherapp.domain.repository.IWeatherRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers

class WeatherViewModel(
    private val weatherRepository: IWeatherRepository
) : ViewModel() {

    private val _weatherLD = MutableLiveData<WeatherUiState>()
    val weatherLD: LiveData<WeatherUiState> = _weatherLD

    private var weatherDisposable: Disposable? = null

    init {
        loadWeather()
    }

    private fun loadWeather() {
        weatherDisposable?.dispose()

        _weatherLD.value = WeatherUiState.Loading
        val hardcodedZip = 98660
        weatherDisposable = weatherRepository.getWeatherObservable(hardcodedZip)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    _weatherLD.value = WeatherUiState.Success(it)
                    Log.e("wtf", "loaded: $it")
                },
                {
                    _weatherLD.value = WeatherUiState.Error(it)
                    Log.e("wtf", "error $it")
                },
                {
                    //nothing to do
                    Log.e("wtf", "completed")
                }
            )
    }

    override fun onCleared() {
        weatherDisposable?.dispose()
        super.onCleared()
    }

    fun reloadWeather() {
        loadWeather()
    }

}

sealed class WeatherUiState {
    data class Success(val data: WeatherDataModel) : WeatherUiState()
    object Loading : WeatherUiState()
    data class Error(val fail: Throwable) : WeatherUiState()
}
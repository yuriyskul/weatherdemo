package com.example.weatherapp.application

import java.util.concurrent.TimeUnit

class WeatherUtil {

    companion object {

        private const val CACHE_VALID_TIME_DURATION_MIN = 10

        fun isCachedTimeExpired(cacheDataTime: Long): Boolean {
            val currentTimeSeconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())
            val timeDifferenceSec = currentTimeSeconds - cacheDataTime
            val timeDifferenceInMinutes = TimeUnit.SECONDS.toMinutes(timeDifferenceSec)
            return timeDifferenceInMinutes > CACHE_VALID_TIME_DURATION_MIN
        }
    }
}


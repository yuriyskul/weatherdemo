package com.example.weatherapp.application

import android.app.Application
import com.example.weatherapp.data.localstorage.AppPrefs
import com.example.weatherapp.data.weather.WeatherApi
import com.example.weatherapp.data.weather.WeatherRetrofitService
import com.example.weatherapp.domain.repository.IWeatherRepository
import com.example.weatherapp.domain.repository.WeatherCacheableRepositoryImpl

class WeatherApp : Application() {

    val appPrefs: AppPrefs by lazy {
        AppPrefs(instance)
    }

    private val weatherRetrofitService: WeatherRetrofitService by lazy {
        WeatherApi.create()
    }

    val weatherRepository: IWeatherRepository by lazy {
        // this is cacheable repository implementation
        WeatherCacheableRepositoryImpl(weatherRetrofitService, appPrefs)
        // to use not cacheable repository - uncomment next line and comment previous
//        WeatherRepositoryImpl(weatherRetrofitService, appPrefs)
    }

    override fun onCreate() {
        instance = this
        super.onCreate()
    }


    companion object {
        lateinit var instance: WeatherApp

        @JvmStatic
        fun get() = instance
    }
}
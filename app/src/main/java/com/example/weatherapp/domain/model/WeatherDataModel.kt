package com.example.weatherapp.domain.model

import com.google.gson.annotations.Expose

data class WeatherDataModel(
    @Expose
    val main: Main,
    @Expose
    val wind: Wind,
    @Expose
    val clouds: Clouds,
    @Expose
    val name: String,
    @Expose
    val cod: Int,
    @Expose
    val dt: Long,
    @Expose
    var zip: Int = -1
) {
}

data class Main(
    @Expose
    val temp: String,
    @Expose
    val feels_like: String,
    @Expose
    val temp_min: String,
    @Expose
    val temp_max: String,
    @Expose
    val pressure: Int,
    @Expose
    val humidity: Int,
)

data class Wind(
    @Expose
    val speed: String,
    @Expose
    val deg: String,
)


data class Clouds(
    @Expose
    val all: String
)
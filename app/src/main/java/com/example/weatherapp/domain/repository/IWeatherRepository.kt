package com.example.weatherapp.domain.repository

import com.example.weatherapp.domain.model.WeatherDataModel
import io.reactivex.rxjava3.core.Observable

interface IWeatherRepository {
    fun getWeatherObservable(zip:Int): Observable<WeatherDataModel>
}
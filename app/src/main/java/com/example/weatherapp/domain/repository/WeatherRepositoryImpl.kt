package com.example.weatherapp.domain.repository

import com.example.weatherapp.data.localstorage.AppPrefs
import com.example.weatherapp.data.weather.WeatherRetrofitService
import com.example.weatherapp.domain.model.WeatherDataModel
import io.reactivex.rxjava3.core.Observable


class WeatherRepositoryImpl(
    private val weatherService: WeatherRetrofitService,
    private val appPrefs: AppPrefs
) : IWeatherRepository {


    override fun getWeatherObservable(zip: Int): Observable<WeatherDataModel> {
        return weatherService.getWeatherForecast(zip)
            .map {
                if (it.cod == 200) {
                    it
                } else {
                    when (it.cod) {
                        // look up api doc and handle different error codes
//                        401 -> throw SomeException("some some")
//                        403 -> throw AnotherException("some some")
//                        500, 502 -> throw InternalServerError()
                        else -> throw Exception("""Something went wrong while loading weather +${it.cod}""")
                    }
                }
            }
    }

}

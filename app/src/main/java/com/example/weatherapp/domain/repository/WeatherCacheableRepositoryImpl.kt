package com.example.weatherapp.domain.repository

import android.util.Log
import com.example.weatherapp.application.WeatherUtil.Companion.isCachedTimeExpired
import com.example.weatherapp.data.localstorage.AppPrefs
import com.example.weatherapp.data.weather.WeatherRetrofitService
import com.example.weatherapp.domain.model.WeatherDataModel
import io.reactivex.rxjava3.core.Observable

class WeatherCacheableRepositoryImpl(
    private val weatherService: WeatherRetrofitService,
    private val appPrefs: AppPrefs
) : IWeatherRepository {

    override fun getWeatherObservable(zip: Int): Observable<WeatherDataModel> {
        return getLocalCachedWeatherObservable(zip)
            .switchIfEmpty(getRemoteWeatherObservable(zip)
                .doOnNext {
                    appPrefs.saveWeather(it)
                }
            )
    }


    //  First, we recommend making API calls no more than once in 10 minutes for each location,
//    whether you call it by city name, geographical coordinates or by zip code.
//    The update frequency of the OpenWeather model is not higher than once in 10 minutes.
    private fun getLocalCachedWeatherObservable(zip: Int): Observable<WeatherDataModel> {
        val weather = appPrefs.getWeather()
        if (weather == null) {
            Log.e("WTF", "local cache is empty")
            return Observable.empty()
        }

        if (zip != weather.zip) {
//            cached weather is assosiated with another location - not actual
            Log.e("WTF", "cache has another zip code - not actual so go to internet and load weather")

            //cached weather data has been expired
            return Observable.empty()
        }

        if (isCachedTimeExpired(weather.dt)) {
            Log.e("WTF", "cache has been expired")

            //cached weather data has been expired
            return Observable.empty()
        }
        Log.e("WTF", "cache data is valid ")

        return Observable.just(weather)
    }


    private fun getRemoteWeatherObservable(zip: Int): Observable<WeatherDataModel> {
        return weatherService.getWeatherForecast(zip)
            .map {
                if (it.cod == 200) {
//                    update data model from server with zip
                    it.apply { it.zip = zip }
                } else {
                    when (it.cod) {
                        // look up api doc and handle different error codes
//                        401 -> throw SomeException("some some")
//                        403 -> throw AnotherException("some some")
//                        500, 502 -> throw InternalServerError()
                        else -> throw Exception("Something went wrong while loading weather +${it.cod}")
                    }
                }
            }
    }
}
package com.example.weatherapp.data.weather

import com.example.weatherapp.domain.model.WeatherDataModel
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Query


// https://api.openweathermap.org/data/2.5/forecast?zip=98660&units=Imperial&appid=<replace with app_id>


interface WeatherRetrofitService {
    @GET(
//        "data/2.5/forecast"
        "data/2.5/weather"
                +"?units=Imperial"
                +"&appid=$WEATHER_API_KEY"
    )
    fun getWeatherForecast(
        @Query("zip") zip: Int,
    ): Observable<WeatherDataModel>
}


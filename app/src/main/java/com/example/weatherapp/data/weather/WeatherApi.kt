package com.example.weatherapp.data.weather

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class WeatherApi {

    companion object {

        fun create(): WeatherRetrofitService {
            val logging = HttpLoggingInterceptor()

            logging.level = HttpLoggingInterceptor.Level.BODY

            val httpClient = OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
            httpClient.addInterceptor(logging)

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                    RxJava3CallAdapterFactory.create()
                )

                .addConverterFactory(
                    GsonConverterFactory.create()
                )
                .baseUrl(WEATHER_RESOURCE_URL)
                .client(httpClient.build())
                .build()

            return retrofit.create(WeatherRetrofitService::class.java)
        }
    }
}



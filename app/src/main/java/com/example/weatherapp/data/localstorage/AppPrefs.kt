package com.example.weatherapp.data.localstorage

import android.content.Context
import android.content.SharedPreferences
import com.example.weatherapp.domain.model.WeatherDataModel
import com.google.gson.GsonBuilder



class AppPrefs(appContext: Context) {

    private var prefs: SharedPreferences =
        appContext.getSharedPreferences(APP_PREFS_FILE_NAME, Context.MODE_PRIVATE)


    fun getWeather(): WeatherDataModel? {
        val gson = GsonBuilder()
            .excludeFieldsWithoutExposeAnnotation()
            .create()
        val json: String = prefs.getString(WEATHER_KEY, null) ?: return null
        return gson.fromJson(json, WeatherDataModel::class.java)
    }

    fun saveWeather(user: WeatherDataModel) {
        val gson = GsonBuilder()
            .excludeFieldsWithoutExposeAnnotation()
            .create()
        val json = gson.toJson(user)
        val editor = prefs.edit()
        editor.putString(WEATHER_KEY, json)
        editor.apply()
    }





    companion object {
        private const val WEATHER_KEY = "cached_weather"

        private val APP_PREFS_FILE_NAME = "app_prefs"
    }
}